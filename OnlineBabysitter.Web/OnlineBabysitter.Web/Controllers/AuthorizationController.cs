﻿
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using OnlineBabysitter.Web.DataLayer;
using OnlineBabysitter.Web.Domain.Entities;
using OnlineBabysitter.Web.Domain.Models;
using OnlineBabysitter.Web.Helpers;
using OnlineBabysitter.Web.Services;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace OnlineBabysitter.Web.Controllers
{

    [ApiController]
    [Route("api/auth")]
    public class AuthorizationController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        public AuthorizationController(IUnitOfWork unitOfWork, IUserService userService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _userService = userService;
            _mapper = mapper;
        }
        [HttpPost("authenticate")]
        public IActionResult Authenticate(LoginUserModel model)
        {
            var response = _userService.Authenticate(model);

            if (response == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(response);
        }
        [HttpPost("register")]
        public IActionResult Register(CreateUserModel model)
        {
            var record = _mapper.Map<UserRecord>(model); 
            _unitOfWork.UserRepository.Add(record);
            _unitOfWork.SaveChanges();
            return Ok();
        }
        [Authorize(UserType.PARENT)]
        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();
            return Ok(users);
        }
        [HttpGet("{id:int}")]
        public IActionResult GetById(int id)
        {
            // only admins can access other user records
            var currentUser = (UserRecord)HttpContext.Items["User"];
            if (id != currentUser?.Id && currentUser?.Type != UserType.PARENT)
                return Unauthorized(new { message = "Unauthorized" });

            var user = _userService.GetById(id);
            return Ok(user);
        }
    }
}
