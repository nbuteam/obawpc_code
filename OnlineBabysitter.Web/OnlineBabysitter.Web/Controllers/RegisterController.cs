﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using OnlineBabysitter.Web.DataLayer;
using OnlineBabysitter.Web.Domain.Entities;
using OnlineBabysitter.Web.Domain.Models;

namespace OnlineBabysitter.Web.Controllers
{
    [Route("api/register")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public RegisterController(IUnitOfWork unitOfWork,
                               IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        [HttpPost("parent")]
        public IActionResult CreateParent(CreateParentModel model)
        {
            var record = _mapper.Map<ParentRecord>(model);
            _unitOfWork.UserRepository.Add(record);
            _unitOfWork.SaveChanges();
            return CreatedAtAction("GetUser", new { record.Id }, record);
        }
        [HttpPost("babysitter")]
        public IActionResult CreateBabysitter(CreateBabysitterModel model)
        {
            var record = _mapper.Map<BabysitterRecord>(model);
            _unitOfWork.UserRepository.Add(record);
            _unitOfWork.SaveChanges();
            return CreatedAtAction("GetUser", new { record.Id }, record);
        }
        [HttpGet("{id}")]
        public IActionResult GetUser(int id)
        {
            var user = _unitOfWork.UserRepository.GetById(id);
            if(user == null)
            {
                return NotFound();
            }
            return Ok("Congratulations! " + user.Name + "is created!");
        }
        [HttpPost]
        public IActionResult CreateUser(UserModel userModel)
        {
            UserRecord record = null;
            if(userModel.UserType == UserType.PARENT)
            {
                record = _mapper.Map<ParentRecord>(userModel);
            }
            else
            {
                record = _mapper.Map<BabysitterRecord>(userModel);
            }
            _unitOfWork.UserRepository.Add(record);
            _unitOfWork.SaveChanges();
            return CreatedAtAction("GetUser", new { record.Id }, record);
        }
        [HttpPut("id")]
        public ActionResult<UserRecord> UpdateUser(int id, UpdateUserModel userModel)
        {
            var userRecord = _mapper.Map<UserRecord>(userModel);
            var existingUser =  _unitOfWork.UserRepository.GetById(id);
            _mapper.Map(userModel, existingUser);
            _unitOfWork.UserRepository.Update(existingUser);
            _unitOfWork.SaveChanges();
            return userRecord;
        }
        [HttpDelete("id")]
        public IActionResult RemoveUser(int id)
        {
            var user = _unitOfWork.UserRepository.GetById(id);
            _unitOfWork.UserRepository.Delete(user);
            _unitOfWork.SaveChanges();
            return Ok("Deleted");
        }


    }
}
