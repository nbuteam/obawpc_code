﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineBabysitter.Web.Domain.Entities
{
    [Table("Children")]
    public class ChildRecord : AbstractEntity
    {
        public string Name { get; set; }
        public string Gender { get; set; }
        public UserRecord Parent { get; set; }
        public int ParentId { get; set; }
    }
}
