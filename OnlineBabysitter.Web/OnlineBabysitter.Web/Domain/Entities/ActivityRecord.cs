﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineBabysitter.Web.Domain.Entities
{
    [Table("activities")]
    public class ActivityRecord : AbstractEntity
    {
        public int ChildId { get; set; }
        public virtual ChildRecord Child { get; set; }
        public DateTime? Time { get; set; }
        public string Description { get; set; }
    }
}
