﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineBabysitter.Web.Domain.Entities
{
    [Table("parents")]
    public class ParentRecord : UserRecord
    {
        public int? BabysitterId { get; set; }
        public virtual BabysitterRecord Babysitter { get; set; }
        public ICollection<ChildRecord> Children { get; set; }
    }
}
