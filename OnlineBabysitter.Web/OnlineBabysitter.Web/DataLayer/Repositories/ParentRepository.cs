﻿using OnlineBabysitter.Web.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineBabysitter.Web.DataLayer.Repositories
{
    public class ParentRepository : AbstractRepository<ParentRecord>, IParentRepository
    {
        public ParentRepository(BabysitterContext context):base(context)
        {

        }
    }
}
