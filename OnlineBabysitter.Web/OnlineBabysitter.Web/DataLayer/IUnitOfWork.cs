﻿using OnlineBabysitter.Web.DataLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineBabysitter.Web.DataLayer
{
    public interface IUnitOfWork:IDisposable
    {
        IUserRepository UserRepository { get; }
        IActivityRepository ActivityRepository { get; }
        IBabysitterRepository babysitterRepository { get; }
        IChildRepository ChildRepository { get; }
        IParentRepository ParentRepository { get; }
        void SaveChanges();
    }
}
