﻿using OnlineBabysitter.Web.DataLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineBabysitter.Web.DataLayer
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BabysitterContext _context;
        private IUserRepository _userRepository;
        private IActivityRepository _activityRepository;
        private IBabysitterRepository _babysitterRepository;
        private IChildRepository _childRepository;
        private IParentRepository _parentRepository;


        public IUserRepository UserRepository
        {
            get
            {
                if(_userRepository == null)
                {
                    _userRepository = new UserRepository(_context);
                }
                return _userRepository;
            }
        }

        public IActivityRepository ActivityRepository
        {
            get
            {
                if (_activityRepository == null)
                {
                    _activityRepository = new ActivityRepository(_context);
                }
                return _activityRepository;
            }
        }

        public IBabysitterRepository babysitterRepository
        {
            get
            {
                if (_babysitterRepository == null)
                {
                    _babysitterRepository = new BabysitterRepository(_context);
                }
                return _babysitterRepository;
            }
        }

        public IChildRepository ChildRepository
        {
            get
            {
                if (_childRepository == null)
                {
                    _childRepository = new ChildRepository(_context);
                }
                return _childRepository;
            }
        }

        public IParentRepository ParentRepository
        {
            get
            {
                if (_parentRepository == null)
                {
                    _parentRepository = new ParentRepository(_context);
                }
                return _parentRepository;
            }
        }

        public UnitOfWork(BabysitterContext context)
        {
            _context = context;
            _userRepository = new UserRepository(_context);
        }
        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
