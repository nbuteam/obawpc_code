﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineBabysitter.Web.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineBabysitter.Web.DataLayer.Configurations
{
    public class ParentConfiguration : IEntityTypeConfiguration<ParentRecord>
    {
        public void Configure(EntityTypeBuilder<ParentRecord> builder)
        {
            builder.ToTable("parents").HasOne(x => x.Babysitter).WithMany().HasForeignKey(y => y.BabysitterId);
        }
    }
}
