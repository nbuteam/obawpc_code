﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineBabysitter.Web.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineBabysitter.Web.DataLayer.Configurations
{
    public class ChildConfiguration : IEntityTypeConfiguration<ChildRecord>
    {
        public void Configure(EntityTypeBuilder<ChildRecord> builder)
        {
            builder.ToTable("children");
        }
    }
}
