﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OnlineBabysitter.Web.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineBabysitter.Web.DataLayer.Configurations
{
    public class ActivityConfiguration : IEntityTypeConfiguration<ActivityRecord>
    {
        public void Configure(EntityTypeBuilder<ActivityRecord> builder)
        {
            builder.ToTable("activites");
        }
    }
}
