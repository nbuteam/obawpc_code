﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using OnlineBabysitter.Web.DataLayer;
using OnlineBabysitter.Web.Domain.Entities;
using OnlineBabysitter.Web.Domain.Models;
using OnlineBabysitter.Web.Helpers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace OnlineBabysitter.Web.Services
{
    public class UserService : IUserService
    {
        private readonly AppSettings _appSettings;
        private readonly IUnitOfWork _unitOfWork;
        private IJwtUtils _jwtUtils;
        public UserService(IOptions<AppSettings> appSettings, IUnitOfWork unitOfWork, IJwtUtils jwtUtils)
        {
            _appSettings = appSettings.Value;
            _unitOfWork = unitOfWork;
            _jwtUtils = jwtUtils;
        }
        public AuthenticateUser Authenticate(LoginUserModel model)
        {
            var user = _unitOfWork.UserRepository.GetWhere(x => x.UserName == model.Username && x.Password == model.Password).FirstOrDefault();

            // return null if user not found
            if (user == null) return null;

            // authentication successful so generate jwt token
            var jwtToken = _jwtUtils.GenerateJwtToken(user);

            return new AuthenticateUser(user, jwtToken);
        }

        public UserRecord GetById(int userId)
        {
            return _unitOfWork.UserRepository.GetById(userId);
        }
        public IEnumerable<UserRecord> GetAll()
        {
            return _unitOfWork.UserRepository.GetAll();
        }
    }
}
