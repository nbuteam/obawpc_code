﻿using OnlineBabysitter.Web.Domain.Entities;
using OnlineBabysitter.Web.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace OnlineBabysitter.Web.Services
{
    public interface IUserService
    {
        AuthenticateUser Authenticate(LoginUserModel model);
        UserRecord GetById(int userId);
        IEnumerable<UserRecord> GetAll();
    }
}
