﻿using AutoMapper;
using OnlineBabysitter.Web.Domain.Entities;
using OnlineBabysitter.Web.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineBabysitter.Web.Extensions
{
    public class ViewModelToRecordProfile:Profile
    {
        public ViewModelToRecordProfile()
        {
            CreateMap<UserModel, UserRecord>();
            CreateMap<UpdateUserModel, UserRecord>();
            CreateMap<CreateParentModel, ParentRecord>();
            CreateMap<CreateBabysitterModel, BabysitterRecord>();
            CreateMap<CreateUserModel, UserRecord>();

        }
    }
}
