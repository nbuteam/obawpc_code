﻿using AutoMapper;
using OnlineBabysitter.Web.Domain.Entities;
using OnlineBabysitter.Web.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineBabysitter.Web.Extensions
{
    public class RecordToViewModelProfile:Profile
    {
        public RecordToViewModelProfile()
        {
            CreateMap<UserRecord, UserModel>();
            CreateMap<ParentRecord, UserModel>();
            CreateMap<BabysitterRecord, UserModel>();
        }
    }
}
