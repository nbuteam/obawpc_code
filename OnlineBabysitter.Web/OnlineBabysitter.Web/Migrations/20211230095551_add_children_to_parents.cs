﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineBabysitter.Web.Migrations
{
    public partial class add_children_to_parents : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ParentRecordId",
                table: "children",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_children_ParentRecordId",
                table: "children",
                column: "ParentRecordId");

            migrationBuilder.AddForeignKey(
                name: "FK_children_parents_ParentRecordId",
                table: "children",
                column: "ParentRecordId",
                principalTable: "parents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_children_parents_ParentRecordId",
                table: "children");

            migrationBuilder.DropIndex(
                name: "IX_children_ParentRecordId",
                table: "children");

            migrationBuilder.DropColumn(
                name: "ParentRecordId",
                table: "children");
        }
    }
}
