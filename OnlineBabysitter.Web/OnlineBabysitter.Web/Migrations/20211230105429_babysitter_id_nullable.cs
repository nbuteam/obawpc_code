﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineBabysitter.Web.Migrations
{
    public partial class babysitter_id_nullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_parents_babysitters_BabysitterId",
                table: "parents");

            migrationBuilder.AlterColumn<int>(
                name: "BabysitterId",
                table: "parents",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_parents_babysitters_BabysitterId",
                table: "parents",
                column: "BabysitterId",
                principalTable: "babysitters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_parents_babysitters_BabysitterId",
                table: "parents");

            migrationBuilder.AlterColumn<int>(
                name: "BabysitterId",
                table: "parents",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_parents_babysitters_BabysitterId",
                table: "parents",
                column: "BabysitterId",
                principalTable: "babysitters",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
