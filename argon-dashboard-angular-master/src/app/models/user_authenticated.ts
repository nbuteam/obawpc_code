import { UserType } from "./user_type";

export class AuthenticatedUser{
        Id : number;
        Name : string;
        Username : string;
        Token? : string;
        Type : UserType;
}