import { UserType } from "./user_type"

export class User{
    id:number;
    firstName:string;
    lastName:string;
    username:string;
    type: UserType;
    token?: string;
}