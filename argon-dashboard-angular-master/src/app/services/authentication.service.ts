import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthenticatedUser } from '../models/user_authenticated';
import { UserType } from '../models/user_type';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private userSubject: BehaviorSubject<AuthenticatedUser>;
  public user: Observable<AuthenticatedUser>;
  constructor(
    private router: Router,
    private http: HttpClient) {
      this.userSubject = new BehaviorSubject<AuthenticatedUser>(JSON.parse(localStorage.getItem('user')));
      this.user = this.userSubject.asObservable();
    }
  public get userValue(): AuthenticatedUser {
    return this.userSubject.value;
  }  

  login(username: string, password: string) {
    return this.http.post<any>(`${environment.apiUrl}/api/auth/authenticate`, { username, password })
        .pipe(map(user => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('user', JSON.stringify(user));
            this.userSubject.next(user);
            return user;
        }));
  }
  register(name: string, address: string, phonenumber: string, username: string, password: string, email: string, type: UserType) {
    return this.http.post<any>(`${environment.apiUrl}/api/auth/register`, { name, address, phonenumber, username, password, email, type })
        .pipe();
  }
  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
    this.userSubject.next(null);
    this.router.navigate(['/login']);
  } 
}
